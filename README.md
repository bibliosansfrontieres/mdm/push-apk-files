# push-apk-files

Cet outil permet d'installer en masse des applications depuis les fichiers
`.apk`.

Il vérifie la présence de l'exécutable `adb`, et le télécharge et l'installe si besoin.

Il vérifie ensuite l'existence d'un dossier contenant les fichiers `.apk`.

Il vérifie ensuite qu'une tablette est connectée avec le débogage USB activé.

Enfin, il procède à l'installation de tous les fichiers `.apk` sur la tablette.


## Utilisation

Single argument: the directory where the APK files are pushed from:

```shell
./push-apk-files <apk_dir>
```

Environment variables can be used:
* `LOGGER_LVL`: can be debug, info, warn
* `DRYRUN`: if set to `1`, tells what would be done but does nothing.
* `APK_LOCAL_REPO`: path to the APK files directory

```shell
LOGGER_LVL=warn DRYRUN=0 APK_LOCAL_REPO=/media/usb/apkfiles push-apk-files
```

## Workflow

* Environnement de travail sous Linux
* [Préparation de la tablette](doc/prepare-tablet.md)
* Lancement du script
* Installation des applications manquantes


#### Installer les apps manquantes

Car pour certaines, on n'a pas pu automatiser. Heureusement il en manque peu :-)

Sur le bureau, un dossier « Recommended applications » fournit un raccourci vers la page de chaque app sur le Google Play Store. Il suffit de cliquer sur chacune de ces icones et de procéder à son installation.

Une fois qu'elles sont toutes installées, on peut supprimer ce dossier (appui long sur le dossier --> Supprimer)

## History

### v0.0.0.1-alpha

Once upon a time, we needed to install a hundred of apps on a hundred of tablets in a very limited time span. A quick and very dirty script was written, basically gathering a few `for` loops. Since then, this coding horror has been burnt, then buried, and we are all happy with that.

### v1.0

This script started as a one-shot tool, needed to install a bunch of apps onto tablets after the devices have been reset. The script was written to be used by non tech savvy people, so it was designed to save as many keystrokes as possible. « Just plug the tablet and run the script! » was the goal (and it worked perfectly).

### v2.0

(work in progress)

For our current needs, the script has several requirements:

* [x] manage several devices
* [ ] pick app from a flat text file list
* [ ] keep a report of performed operations (and errors if any)
